// esmodule, serverless semantle implementation
// to access remote sqlite database directly

import { load } from "../../sqljs/sql-httpvfs.js";

const VEC_ELEMENTS = 300, VEC_BYTES = VEC_ELEMENTS * 4;

class SQLBackend extends RemoteBackend {
    constructor(sql) {
        super();
        this.sql = sql;
    }

    async getSimilarityStory(secret) {
        try {
            let con = this.sql.db,
                res = await con.query("SELECT top, top10, rest FROM similarity_range WHERE word = ?", [secret]);
            return res[0];
        } catch (e) {
            return null;
        }
    }

    async getModel(secret, word) {
        let con = this.sql.db,
            res = await con.query(`SELECT vec, percentile 
                FROM word2vec left outer join nearby
                ON nearby.word=? AND nearby.neighbor=? 
                WHERE word2vec.word = ?`, [secret, word, word]);
        if (res.length === 0) {
            return "";
        }
        // expand bfloat16 to float32
        let row = res[0],
            vec = row.vec,
            fullVec = new Uint8Array(VEC_BYTES),
            asFloat = new DataView(fullVec.buffer),
            result = { vec: [] };
        for (let i = 0; i < VEC_ELEMENTS; i++) {
            fullVec[i * 4 + 2] = vec[i * 2];
            fullVec[i * 4 + 3] = vec[i * 2 + 1];
        }
        for (let i = 0; i < VEC_ELEMENTS; i++) {
            result.vec[i] = asFloat.getFloat32(i * 4, true);
        }
        if (row.percentile != null) {
            result.percentile = row.percentile;
        }
        return result;
    }

    async getNearby(word) {
        try {
            let con = this.sql.db,
                res = await con.query("SELECT neighbor FROM nearby WHERE word = ? order by percentile desc limit 10 offset 1", [word]),
                neighbors = res.map(row => row.neighbor);
            return neighbors;
        } catch (e) {
            return null;
        }
    }
};

let sql = await load(window.location.pathname + "word2vec.db"), backend = new SQLBackend(sql);

window.sql = sql;
window.backend = backend;

// is there a better way to know whether the traditional script is ready?
function init() { Semantle.init(backend); };

if (document.readyState === 'complete') {
    init();
} else {
    window.addEventListener('load', init);
}